+++
draft = false
image = "images/study/study.jpg"
date = "2020-04-05"
title =  "Study Less To Get Better Results"
weight = 1
description = "This article will teach you how to optimize your study method. Increasing the efficiently of your study will lead to  better grades at school, understanding key concepts in less time and consequently enjoying more free time. Optimize study method, plan, focus, overthinking, make it interesting, Master it, exam. Written by Mattia Canevascini."
author = "Mattia Canevascini"
+++


This article will teach you how to optimize your study method. Increasing the efficiently of your study will lead to  better grades at school, understanding key concepts in less time and consequently enjoying more free time.

<!--more-->

## The Plan 
Plan a chunk of uninterrupted time to dedicate to the task. It could be 25 minutes or 2 hours of homework, reading a book, programming, writing etc.

Set a specific attainable goal, reading one chapter, doing the exercises one and two or understanding a specific concept are good examples. Setting goals will make your progress tangible and more rewarding.

Block any possible distractions, turn on airplane mode on your phone and disable notifications. Avoid having food nearby and snacking.

These methods will make your time as productive as possible that is how in the end you will need less time to achieve your study goals.

## Focus
Being able to remain concentrated is a great ability and can be difficult for many people. Avoiding distractions as described above will help a lot but generally this is a skill to learn and develop with time.

An effective method to improve concentration is to analyse your study environment. Studying in your room where you usually play video games or check social networks will be difficult because your mind associates this environment with leisure time. Many people prefer studying in the library because the environment library gets associated with work and study, so it is easier for them to concentrate. 

If you want to improve your concentration, try to change your study environment in one that you do not associate with distractions.

## Do not think too much
When you study keep your mind quite and avoid overthinking. Develop a positive mindset, no matter how difficult it is or the amount of work you must accomplish, say to yourself that you will do your best and that it will be enough.

## Make it interesting 
Learning something that you enjoy, a new sport or a subject of study that you find particularly interesting for example it is way easier that learning something that you do not like. If you do not particularly like the subject, you must make it as interesting as possible by yourself. You could make a link between the subject of study and your interests, ask question to the teacher or to somebody that has a greater knowledge than you or find real-world applications of the subject.

## Master it
If you want to master the subject, there is an extra step that you must take. You must understand the subject so well that you can explain it to others. Explain it to people who does not have any understanding of it and to people of your same or greater level of knowledge. This process will make you spot all the gaps in your knowledge that you were not aware of. Correcting those gaps will prefect your knowledge. 

## The Exam
The day of the exams you should be confident about your newly acquired knowledge but most importantly you should be well slept. By having a good night of sleep, you will be sharp and you will maximise your result.

During the exam you must make sure to be relaxed and not panic, repeat to yourself that you have studied correctly and you have acquired the knowledge to get a good grade. Don’t let anxiety and/or an unconstructive thinking ruin your exam.

If you see that the exam is infinitely long or it’s not what you have expected think that you will do the best anyway. If you can, prioritize the exercises/questions that you know you can do correctly and later do the rest.

## In Conclusion
Try each of the steps described and you will optimize your study session, need less time to understand concepts and get better grades. 

&nbsp;

**Written by Mattia Canevascini**
