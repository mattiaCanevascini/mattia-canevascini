---
title: "Cure acne in days"
date: 2020-04-06T07:11:02+02:00
image: "/images/acne/acneDC.jpg"
draft: true
weight: 2
description: "In this article I present to you an effective method to address acne. This method does not involve the use of creams, lotions, supplements or medicine, instead it addresses the source of your acne, eliminating it. By Mattia Canevascini"
author: "Mattia Canevascini"
---

In this article I present to you an effective method to address acne. This method does not involve the use of creams, lotions, supplements or medicine, instead it addresses the source of your acne, eliminating it.
<!--more-->
I suffered myself from an acute form of acne, I tried conventional treatment with creams, lotions, and strong pills. The medicine I have taken slowly cleared my skin but when I stopped the treatment the acne came back as strong as before.
 
This situation led me to research a solution on my own. After having discovered, understood and applied the method that I describe in this article, pimples and cysts stopped showing up and my skin started to heal.

## Source cause 
For most peoples acne is caused by an unhealthy diet. The body responds to the toxicity of food by getting inflamed and breaking out. Eliminating inflammatory foods is the first thing to do when addressing acne seriously and it is often enough to clear up acne completely. 

In other cases, in addition to toxic food, acne can be caused by dairy products or by foods with a high glycemic index. Those foods can be inflammatory for certain people and totally fine for others.

The use of topical products, creams, lotions and moisturizers is not healpfull. If the source cause is not corrected acne will persist. I recommend to not touch your face as much as possible. By not scratching yourself you will not create more damage to your skin. 

## Eliminate it
Eliminating toxic food from your diet can be the single change that you will need to make to clear up your acne. As you will understand, eating healthy does not mean eating more salad or eating more whole grains. Eating healthy means firstly avoiding toxic food and secondly focusing on natural and nutrient dense foods. 

So how do we define if a food is toxic or not? Generally, every **processed food** should be avoided but the main toxic ingredients can be categorized in:
 
- **Vegetable oils** such as margarine, sunflower seed oil, canola oil and palm oil are toxic. They cause inflammation and multiple disease.

- **Sugars**, white, brown, dextrose, fructose, high fructose corn syrup are also toxic.

Carbohydrate intake should be limited. Many individuals find themselves reacting to and/or grains. Limit or eliminate completely high carbohydrate foods such as pasta, bread, rice, white potatoes, ecc.

## Nutritious food
Once you have understood what foods you should avoid it is important to now understand what food to focus on. Your body needs quality nutrition so you should eat high quality nutrient dense foods. 

High quality animal products and seasonal vegetables should be your staple. Foods like meat, fish, eggs, cheese, fruits and vegetables will give you the best nutrition. These foods will satiate you; they will provide plenty of energy and nutrients to make you function at your peak.

## Dairy
Conventional dairy is inflammatory for many people, to understand why it is necessary to look at the difference between natural dairy and our conventional pasteurized and homogenized dairy. 

By rule of thumb, the more a food is processed, the more it is poisonous for us. The same logic goes for dairy. Pasteurization and homogenization kill enzymes naturally occurring in milk making it way more difficult to digest. 

I recommend eating only high quality raw dairy products, for example cheeses like Parmigiano-Reggiano, Grana Padano and Gruyere. Some people that consider themselves as lactose intolerant can actually digest raw dairy products very well.

If you have fixed your diet in the way I describe and you are still suffering from acne, stop eating dairy products for a week and see if you get better. Do not replace dairy with fake milks such as oats, rice or soy milk as they are processed, high in sugars and unhealthy.

## Guaranteed results
This is it; it could be the last acne related article that you will need to read.  

I can guarantee that if you follow the principle described your skin will get better. Avoiding inflammation and getting adequate nutrition will get your body in strong shape. Remember that no inflammation means no acne. 

If you want to do more research about this way of eating you can look into diets that are very similar, for example low carb, keto, paleo and Weston A. Price diets.

For more information feel free to contact me.

&nbsp;

**Written by Mattia Canevascini**

