+++
title = "Resolve sleep problems tonight"
date = "2020-03-07"
image = "images/sleep/sleepDC.jpg"
weight = 3
description = "Sleep problems can easily be resolved by identifying and correcting the root cause. In this article I will present the common causes and how to correct them. I will also explain advanced techniques if fixing the common causes is not sufficient. Caffeine, Light, Blue light, Body, Physical Acrivity, Junk Food, Keto, Paleo, Weston A Price, Polyphasic sleep. Writte by Mattia Canevascini."
author = "Mattia Canevascini"
+++


Today it is common to suffer from different kind of sleep problems. The main ones are a difficult time falling asleep and waking up multiple times during the night. Sleep problems can easily be resolved by identifying and correcting the root cause. In this article I will present the common causes and how to correct them. I will also explain advanced techniques if fixing the common causes is not sufficient.
<!--more-->
 
### Caffeine
<img src="/images/sleep/coffee.png" alt="coffee" width="800" height="50"/>

Caffeine is the most common sleep disruptor. Caffeine is contained not only in coffee but also in energy drinks and teas. To have a healthy night of sleep drink your last caffeinated drink at lunch. Avoid any type of stimulants after lunch as caffeine does take many hours to be fully metabolized.

### Light
Light is one of the indicators that the body uses to determinate if it is time to produce melatonin and sleep or be awake. Generally, when exposed to light the body become awake and alert, contrary in dim environment it gets tired and sleepy.
 
Artificial light is very similar to sun light. That is why staring at screens in the evening will block the production of melatonin and consequently disrupt your sleep. To get a healthy night of sleep it is crucial to limit or avoid the use of electronic devices such as smartphones and computers in the evening. 
An effective alternative that allows you to use electronic devices is installing a blue light blocking software on your device or wearing blue light blocking glasses. Nevertheless, I recommend stop using your device 30min to a couple of hours before going to sleep.
 
The lighting in your environment is also important. During the evening dim the lights and use light bulbs that emit warm light.
During the night the bedroom has to be in total darkness, block any light that is coming from the outside and lights from electronic devices in your room.

### Body
Do some kind of physical activity during the day, you do not have to run 10km, but taking a walk or doing a kind of sport that you enjoy will make your body require more sleep.

Before you go to sleep pay attention to your body, if you feel wired or stressed take a couple moments to actively relax. Breathing with your belly for a couple of minutes will calm you down.

### Advanced techniques
Before considering the advanced techniques be sure to have fully understood and corrected the common causes explained above. Trying one of the following advanced techniques without having corrected the common causes will be useless.

The first thing to address if you are still having problems is diet and lifestyle. An unhealthy diet and lifestyle can make your body inflamed and sick. I recommend stopping any habits that you know is not conducive to a healthy live. Smoking, heavy drinking and eating junk food for example. If you are willing to take this one step further, I recommend following a paleo / keto / Weston A. Price diet to lower inflammation.
 
Additionally, you may consider looking into polyphasic sleep and trying a biphasic pattern to increase sleep efficiency. I would also suggest giving yourself less time to sleep. It’s useless to give yourself 8 hours to sleep if you will only sleep for 6 hours. Your body could instead get used to sleeping 6 hour and increase the quality of that sleep.

&nbsp;

**Written by Mattia Canevascini**
