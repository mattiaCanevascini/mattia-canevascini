---
title: "Libri"
date: 2020-10-27T15:24:04+01:00
draft: false
description: "I migliori libri che ho letto. Romanzi. Psicologia. Alimentazione. Campi elettro magnetici. Redatto da Mattia Canevascini"
author: "Mattia Canevascini"
---

Una lista dei migliori libri che ho letto.
<!--more-->

### Romanzi 
The Name of the Wind, Patrick Rothfuss

The Wise Man's Fear, Patrick Rothfuss

Il barone rampante, Italo calvino

Blood Song, Anthony Ryan

Per chi suona la campana, Ernest Hemingway 

The Alchemist, Paul Coelho

### Psicologia
The Subtle Art of Not Giving a F*uck, Mark Manson

So good they can't ignore you, Cal Newport 

Think and grow rich, Napoleon Hill

### Alimentazione 
The Story of the Human Body: Evolution, Health, and Disease, Daiel E. Lieberman

Nutrition and physical degeneration, Weston A. Price

Nourishing traditions, Sally Fallon

The fat of the land, Vilhjálmur Stefánsson

### EMF
The non tinfoil guide to EMFs, Nicolas Pineault

&nbsp;

**Redatto da Mattia Canevascini**
