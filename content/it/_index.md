---
date: 2021-8-23
Tags: []
draft: false
Description: "Ciao! Mi occupo di ingegneria elettronica, insegnamento e ricerca indipendente."
author: "Mattia Canevascini"
---


# Ciao! ✌️
### Mi occupo di ingegneria elettronica, insegnamento e ricerca indipendente.

### Formazione
Ad ottobre riceverò il diploma in ingegneria elettronica dall'università HES-SO di Friborgo.

### Lezioni private
Do  lezioni private di 
- Matematica
- Fisica
- Elettronica
- Elettrotecnica

per studenti delle scuole obbligatorie, delle scuole professionali, dell'apprendistato e del liceo.

### Ricerca indipendente
Attualmente sto scrivendo un libro sulla cura olistica dell'acne. Gli articoli e le risorse che ho redatto sono pubblicati su questo sito. 

### Contattami
Sentiti libero di contattarmi tramite email:
mattiacanevascini@tutanota.com
